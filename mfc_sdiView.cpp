
// mfc_sdiView.cpp : Cmfc_sdiView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "mfc_sdi.h"
#endif

#include "mfc_sdiDoc.h"
#include "mfc_sdiView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cmfc_sdiView

IMPLEMENT_DYNCREATE(Cmfc_sdiView, CView)

BEGIN_MESSAGE_MAP(Cmfc_sdiView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &Cmfc_sdiView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// Cmfc_sdiView 构造/析构

Cmfc_sdiView::Cmfc_sdiView()
{
	// TODO: 在此处添加构造代码

}

Cmfc_sdiView::~Cmfc_sdiView()
{
}

BOOL Cmfc_sdiView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// Cmfc_sdiView 绘制

void Cmfc_sdiView::OnDraw(CDC* pDC/*pDC*/)
{
	Cmfc_sdiDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	pDC->TextOutW(100,100,L"这个世界很屌");
}


// Cmfc_sdiView 打印


void Cmfc_sdiView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL Cmfc_sdiView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void Cmfc_sdiView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void Cmfc_sdiView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}

void Cmfc_sdiView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void Cmfc_sdiView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// Cmfc_sdiView 诊断

#ifdef _DEBUG
void Cmfc_sdiView::AssertValid() const
{
	CView::AssertValid();
}

void Cmfc_sdiView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

Cmfc_sdiDoc* Cmfc_sdiView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(Cmfc_sdiDoc)));
	return (Cmfc_sdiDoc*)m_pDocument;
}
#endif //_DEBUG


// Cmfc_sdiView 消息处理程序
